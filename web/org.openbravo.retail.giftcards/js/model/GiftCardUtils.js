/*
 ************************************************************************************
 * Copyright (C) 2012-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _, $, GCNV */


(function () {

  var isGiftCard;

  OB.OBGCNE = OB.OBGCNE || {};
  OB.OBGCNE.Utils = OB.OBGCNE.Utils || {};
  OB.OBGCNE.Utils.PrintCreditNoteTemplate = '../org.openbravo.retail.giftcards/res/creditnote.xml';

  var findProductModel = function (id, callback) {
      var criteria;
      if (!OB.MobileApp.model.hasPermission('OBPOS_remote.product', true)) {
        criteria = {
          'id': id
        };
      } else {
        criteria = {};
        var remoteCriteria = [];
        var productId = {
          columns: ['id'],
          operator: 'equals',
          value: id,
          isId: true
        };
        remoteCriteria.push(productId);
        criteria.remoteFilters = remoteCriteria;
      }

      OB.Dal.find(OB.Model.Product, criteria, function successCallbackProducts(dataProducts) {
        if (dataProducts && dataProducts.length > 0) {
          callback(dataProducts.at(0));
        } else {
          callback(null);
        }
      }, function errorCallback(tx, error) {
        OB.UTIL.showError(error);
        callback(null);
      });
      };

  var findGiftCardModel = function (id, callback) {
      var criteria;
      if (!OB.MobileApp.model.hasPermission('OBPOS_remote.product', true)) {
        criteria = {
          'id': id
        };
      } else {
        criteria = {};
        var remoteCriteria = [],
            productId = {
            columns: ['id'],
            operator: 'equals',
            value: id,
            isId: true
            };
        remoteCriteria.push(productId);
        criteria.remoteFilters = remoteCriteria;
      }
      OB.Dal.find(OB.Model.GiftCard, criteria, function successCallbackGiftCard(dataGiftCard) {
        if (dataGiftCard && dataGiftCard.length > 0) {
          callback(dataGiftCard.at(0));
        } else {
          callback(null);
        }
      }, function errorCallback(tx, error) {
        OB.UTIL.showError(error);
        callback(null);
      });
      };

  var service = function (source, dataparams, callback, callbackError) {
      var process = new OB.DS.Process(source);
      process.exec(dataparams, function (data) {
        if (data && data.exception) {
          callbackError(data);
        } else if (data) {
          callback(data);
        }
      }, callbackError, true, 30000);
      };

  var cancelGiftCard = function (keyboard, receipt, card, success, fail) {

      if (!receipt.get('isEditable')) {
        keyboard.doShowPopup({
          popup: 'modalNotEditableOrder'
        });
        if (fail) {
          fail();
        }
        return;
      }

      OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.CancelGiftCard', {
        giftcard: card,
        _tryCentralFromStore: true,
        _executeInOneServer: true
      }, function (result) {
        OB.UI.GiftCardUtils.findProductModel(result.product.id, function (transactionproduct) {

          // Add properties to product.
          transactionproduct.set('giftCardTransaction', result.transaction.id);
          transactionproduct.set('isEditablePrice', false);
          transactionproduct.set('isEditableQty', false);
          transactionproduct.set('standardPrice', result.transaction.amount);
          transactionproduct.set('ignorePromotions', true);

          keyboard.doAddProduct({
            product: transactionproduct,
            ignoreStockTab: true
          });
        });
        if (success) {
          success();
        }
      }, function (error) {
        var msgsplit = (error.exception.message || 'GCNV_ErrorGenericMessage').split(':');
        keyboard.doShowPopup({
          popup: 'GCNV_UI_Message',
          args: {
            message: OB.I18N.getLabel(msgsplit[0], msgsplit.slice(1))
          }
        });
        if (fail) {
          fail();
        }
      });

      };

  var showUnexpectedError = function() {
    OB.UTIL.showConfirmation.display(
      OB.I18N.getLabel('OBMOBC_WrongUpdate_header'),
      OB.I18N.getLabel('OBMOBC_WrongUpdate_body'),
      [
        {
          label: OB.I18N.getLabel('OBMOBC_LblOk'),
          isConfirmButton: true
        }
      ],
      {
        autoDismiss: false,
        closeOnEscKey: false,
        hideCloseButton: true,
        onHideFunction: function() {
          window.location.reload();
          return true;
        }
      }
    );
  };


  var consumeGiftCardAmount = function (keyboard, receipt, card, amount, includingTaxes, success, fail) {

      if (keyboard && OB.UTIL.isNullOrUndefined(keyboard.model)) {
        if (fail) {
          fail();
        }
        OB.error(
        'keyboard model not found when consuming gift card: ' +
          card.searchKey +
          '; stacktrace:' +
          OB.UTIL.getStackTrace('', false)
	);
        showUnexpectedError();
        return;
      }
      if (!receipt.get('isEditable') && !receipt.get('cancelLayaway') && card.type !== 'BasedOnGLItem') {
        keyboard.doShowPopup({
          popup: 'modalNotEditableOrder'
        });
        if (fail) {
          fail();
        }
        return;
      }

      // For verified returns and line returns the total amount of the line where gift card is consumed must be negative and the transaction in the ERP also will be negative
      // For classic returns (Return this receipt) the total amount of the line where gift card is consumed (in this case we are adding money instead of consume) should be positive, however the transaction in the ERP should be negative (we are increasing the balance) so this value will be processed in the backend to create a proper transaction and then proccessed back again in the client to have the desired value (negative)
      var isMultiOrder = false,
          isReturn = false;
      if (receipt.get('multiOrdersList')) {
        isMultiOrder = true;
      } else if (receipt.getOrderType() === 1) {
        isReturn = true;
      }

      if (receipt.getPaymentStatus().isNegative && !isMultiOrder && !isReturn) {
        amount = -1 * amount;
      }

      var payment = _.find(receipt.get('payments').models, function (p) {
        return p.get('kind') === 'OBPOS_payment.giftcard:' + card.searchKey;
      });

      var giftCardPaymentMethod = _.find(OB.MobileApp.model.get('payments'), function (payment) {
        return payment.payment.searchKey === 'OBPOS_payment.giftcard';
      });
      var newTransactionId = OB.UTIL.get_UUID();

      function cbkConsumeGc() {
        var realAmt = card.currentamount > amount ? OB.DEC.abs(amount) : OB.DEC.abs(card.currentamount);
        if (card.product) {
          OB.UI.GiftCardUtils.findProductModel(card.product, function (
          transactionproduct) {
            // Add properties to product.
            transactionproduct.set('giftCardTransaction', newTransactionId);
            transactionproduct.set('cardId', card.id);
            transactionproduct.set('isEditablePrice', false);
            transactionproduct.set('isEditableQty', false);
            transactionproduct.set('standardPrice', -amount);
            transactionproduct.set('ignorePromotions', true);
            transactionproduct.set('currentamt', -realAmt);
            transactionproduct.set('isReturn', isReturn);

            keyboard.doAddProduct({
              product: transactionproduct,
              ignoreStockTab: true,
              callback: function (success) {
                OB.info('[ticket21491] GCConsumed - callbackAddProd - receipt ' + OB.MobileApp.model.receipt.get('documentNo') + ' - consumed CardSearchKey: ' +card.searchKey + ' - callback-success: '+success);
                if (success === false) {
                  OB.UI.GiftCardUtils.cancelGiftCardTransaction(
                  keyboard, newTransactionId);
                }
              }
            });
          });
        } else {
          var p = _.find(OB.MobileApp.model.get('payments'), function (payment) {
            return (
            giftCardPaymentMethod.paymentMethod.paymentMethod === payment.paymentMethod.paymentMethod);
          });

          // Add Payment
          var modelToApply;
          if (keyboard.model.get('leftColumnViewManager').isOrder()) {
            modelToApply = keyboard.model.get('order');
          } else {
            modelToApply = keyboard.model.get('multiOrders');
          }
          OB.info('[ticket21491] GCConsumed- receipt ' + OB.MobileApp.model.receipt.get('documentNo') + ' - consumed CardSearchKey: ' +card.searchKey);
          modelToApply.addPayment(
          new OB.Model.PaymentLine({
            kind: p.payment.searchKey,
            paymentData: {
              card: card.searchKey,
              cardId: card.id,
              voidTransaction: function (callback) {
                callback(false, null);
              },
              voidConfirmation: false
            },
            name: OB.I18N.getLabel('GCNV_LblGiftCardsCertificate') + ' ' + card.searchKey,
            amount: OB.DEC.abs(realAmt),
            rate: p.rate,
            mulrate: p.mulrate,
            isocode: p.isocode,
            isCash: p.paymentMethod.isCash,
            allowOpenDrawer: p.paymentMethod.allowOpenDrawer,
            openDrawer: p.paymentMethod.openDrawer,
            printtwice: p.paymentMethod.printtwice,
            transaction: newTransactionId
          }));
        }
        if (success) {
          success();
        }
      }
      OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.ConsumeGiftCardAmount', {
        giftcard: card.searchKey,
        _executeInOneServer: true,
        _tryCentralFromStore: true,
        amount: amount,
        isReturn: isReturn,
        transaction: payment ? payment.get('transaction') : null,
        hasPaymentMethod: giftCardPaymentMethod !== undefined && giftCardPaymentMethod !== null,
        newTransactionId: newTransactionId
      }, function (result) {
        OB.info('[ticket21491] GCConsumed- consume-callbackSuccess - receipt ' + OB.MobileApp.model.receipt.get('documentNo') + ' - consumed CardSearchKey: ' +card.searchKey);
        cbkConsumeGc();
      }, function (error) {
        OB.info('[ticket21491] GCConsumed- consume-callbackError - receipt ' + OB.MobileApp.model.receipt.get('documentNo') + ' - consumed CardSearchKey: ' +card.searchKey);
        cbkConsumeGc();
      });
      };

  var checkIfExpiredGiftCardAndConsume = function (keyboard, receipt, card, amount, includingTaxes, success, fail) {
      OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.FindGiftCard', {
        giftcard: card.searchKey,
        giftcardtype: 'G'
      }, function (result) {
        if (result && result.data && result.data.obgcneExpirationdate && OB.OBGCNE.Utils.isInThePast(result.data.obgcneExpirationdate)) {
          var errorMessage = {
            message: OB.I18N.getLabel('OBGCNE_GiftCardExpired', [card.searchKey]),
            keyboard: keyboard
          };
          fail(errorMessage);
        } else {
          consumeGiftCardAmount(keyboard, receipt, card, amount, includingTaxes, success, fail);
        }
      }, function (error) {
        consumeGiftCardAmount(keyboard, receipt, card, amount, includingTaxes, success, fail);
      });
      };

  var consumeGiftCard = function (keyboard, amount, giftCardType) {
      var header = giftCardType === 'BasedOnProductGiftCard' ? 'GCNV_HeaderGiftCard' : 'GCNV_HeaderGiftCertificate';
      OB.info('Giftcard - Popup open, keyboard.model exists:' + (keyboard.model ? true : false) + ', amount: ' + amount + ' Stacktrace: ' + OB.UTIL.getStackTrace('', false));
      keyboard.doShowPopup({
        popup: 'GCNV_UI_ModalGiftCards',
        args: {
          keyboard: keyboard,
          header: OB.I18N.getLabel(header, [OB.I18N.formatCurrency(amount)]),
          giftcardtype: giftCardType,
          amount: amount,
          // Gift Card
          action: function (dialog, consumeOK, consumeFail) {
            OB.info('Giftcard - Apply, keyboard.model exists:' + (keyboard.model ? true : false) + ', giftcard: ' + dialog.args.giftcard + ' Stacktrace: ' + OB.UTIL.getStackTrace('', false));
            var successCallback = function () {
                consumeOK();
                },
                errorCallback = function (errorMessage) {
                consumeFail(null, errorMessage);
                };
            // Pay with a gift card
            OB.UI.GiftCardUtils.checkIfExpiredGiftCardAndConsume(keyboard, keyboard.receipt, dialog.args.giftcard, amount, keyboard.receipt.get('priceIncludesTax'), successCallback, errorCallback);
          }
        }
      });
      };

  var consumeCreditNoteAmount = function (keyboard, receipt, card, amount, paymentToAdd, success, fail, cardId) {

      if (keyboard && OB.UTIL.isNullOrUndefined(keyboard.model)) {
        if (fail) {
          fail();
        }
        OB.error(
          'keyboard model not found when consuming credit note: ' +
            card +
            '; stacktrace:' +
            OB.UTIL.getStackTrace('', false)
        );
	showUnexpectedError();
        return;
      }

      var isMultiOrder = false,
          isReturn = false;
      if (receipt.get('multiOrdersList')) {
        isMultiOrder = true;
      } else if (receipt.getOrderType() === 1) {
        isReturn = true;
      }

      if (receipt.getPaymentStatus().isNegative && !isMultiOrder && !isReturn) {
        amount = -1 * amount;
      }

      var payment = _.find(receipt.get('payments').models, function (p) {
        return p.get('kind') === 'GCNV_payment.creditnote:' + card;
      });

      var giftCardPaymentMethod = _.find(OB.MobileApp.model.get('payments'), function (payment) {
        return payment.payment.searchKey === 'GCNV_payment.creditnote';
      });

      var newTransactionId = payment ? payment.get('transaction') : OB.UTIL.get_UUID(),
          p = giftCardPaymentMethod;

      function resultConsumeCreditNoteAmount(trx, amt, gftSk, gftId) {
        var newPayment = new Backbone.Model();

        // Add Payment
        var modelToApply, paymentData = paymentToAdd.get('paymentData') || {};
        if (keyboard.model.get('leftColumnViewManager').isOrder()) {
          modelToApply = keyboard.model.get('order');
        } else {
          modelToApply = keyboard.model.get('multiOrders');
        }
        OB.UTIL.clone(paymentToAdd, newPayment);
        paymentData.card = gftSk;
        paymentData.cardId = gftId;
        paymentData.voidTransaction = function (callback) {
          callback(false, null);
        };
        paymentData.voidConfirmation = false;
        newPayment.set('name', OB.MobileApp.model.getPaymentName(p.payment.searchKey) + ' ' + card);
        newPayment.set('amount', paymentData.creditNote.currentamount > amt ? OB.DEC.abs(amt) : OB.DEC.abs(paymentData.creditNote.currentamount));
        newPayment.set('paymentData', paymentData);
        newPayment.set('transaction', trx);
        modelToApply.addPayment(new OB.Model.PaymentLine(newPayment.attributes));
        if (success) {
          success();
        }
      }

      OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.ConsumeGiftCardAmount', {
        giftcard: card,
        _executeInOneServer: true,
        _tryCentralFromStore: true,
        amount: amount,
        isReturn: isReturn,
        transaction: payment ? payment.get('transaction') : null,
        newTransactionId: newTransactionId,
        hasPaymentMethod: giftCardPaymentMethod !== undefined && giftCardPaymentMethod !== null
      }, function (result) {
        resultConsumeCreditNoteAmount(newTransactionId, amount, card, cardId);
      }, function (error) {
        resultConsumeCreditNoteAmount(newTransactionId, amount, card, cardId);
      });
      };

  var consumeGiftCardLines = function (keyboard, receipt, card, success, fail, options) {

      if (!receipt.get('isEditable')) {
        keyboard.doShowPopup({
          popup: 'modalNotEditableOrder'
        });
        if (fail) {
          fail();
        }
        return;
      }

      // Calculate the vouchers already used in the lines.
      var currentvouchers = {};
      _.each(receipt.get('lines').models, function (line) {
        var giftvoucherproduct = line.get('product').get('giftVoucherProduct');
        if (giftvoucherproduct) {
          var giftvoucherqty = line.get('product').get('giftVoucherQuantity');
          var voucherinfo = currentvouchers[giftvoucherproduct];
          if (!voucherinfo) {
            voucherinfo = {
              product: giftvoucherproduct,
              quantity: 0
            };
            currentvouchers[giftvoucherproduct] = voucherinfo;
          }
          voucherinfo.quantity += giftvoucherqty;
        }
      }, this);

      var lines = [];
      _.each(receipt.get('lines').models, function (line) {
        var product = line.get('product').get('id');
        var quantity = line.get('qty');
        var promotions = line.get('promotions');
        var discountedPrices = 0;
        var linePrice = 0;
        var voucherinfo = currentvouchers[product];
        if (voucherinfo) {
          quantity -= voucherinfo.quantity;
        }
        _.each(promotions, function (promotion) {
          discountedPrices = discountedPrices + promotion.amt;
        });
        linePrice = (line.get('price') * quantity - discountedPrices) / quantity;
        lines.push({
          product: product,
          quantity: quantity,
          price: linePrice
        });
      }, this);

      OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.ConsumeGiftCardLines', {
        giftcard: card,
        _executeInOneServer: true,
        _tryCentralFromStore: true,
        lines: lines,
        isReturn: receipt.get('documentType') === OB.MobileApp.model.get('terminal').terminalType.documentTypeForReturns
      }, function (result) {
        var data = result.data;
        if (data && (data.exception || data.length === 0)) {
          var params = {
            message: OB.I18N.getLabel('GCNV_ErrorGiftVoucherNotApplied')
          };
          if (options && options.cardType) {
            if (options.cardType === 'V') {
              params.header = OB.I18N.getLabel('GCNV_LblDialogGiftVoucher');
            }
          }
          keyboard.doShowPopup({
            popup: 'GCNV_UI_Message',
            args: params
          });
        } else {
          _.each(data, function (item) {
            OB.UI.GiftCardUtils.findProductModel(item.product.id, function (transactionproduct) {
              // Add properties to product.
              if (item.transaction.quantity < 0) {
                //is a return but we need to add line as usual
                item.transaction.quantity = item.transaction.quantity * -1;
              }
              transactionproduct.set('giftCardTransaction', item.transaction.id);
              transactionproduct.set('giftVoucherProduct', item.transaction.product);
              transactionproduct.set('giftVoucherQuantity', item.transaction.quantity);
              transactionproduct.set('isEditablePrice', false);
              transactionproduct.set('isEditableQty', false);
              transactionproduct.set('standardPrice', -item.price);
              transactionproduct.set('ignorePromotions', true);
              transactionproduct.set('avoidSplitProduct', true);

              var index = 0,
                  receiptlines = receipt.get('lines');

              while (index < receiptlines.length) {
                if (receiptlines.at(index++).get('product').get('id') === item.transaction.product) {
                  break;
                }
              }

              keyboard.doAddProduct({
                product: transactionproduct,
                qty: item.transaction.quantity,
                ignoreStockTab: true,
                options: {
                  at: index
                }
              });
            });
          }, this);
        }

        if (success) {
          success();
        }
      }, function (error) {
        var msgsplit = (error.exception.message || 'GCNV_ErrorGenericMessage').split(':');
        var errorMessage = {
          message: OB.I18N.getLabel(msgsplit[0], msgsplit.slice(1)),
          keyboard: keyboard
        };
        if (options && options.cardType) {
          if (options.cardType === 'V') {
            errorMessage.header = OB.I18N.getLabel('GCNV_LblDialogGiftVoucher');
            errorMessage.message = OB.I18N.getLabel('GCNV_ErrorVoucherNotFound', [msgsplit.slice(1)]);
          }
        }

        if (fail) {
          fail(errorMessage);
        }
        OB.MobileApp.model.triggerOnLine();
        OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.RevertGiftCardAmount', {
          _executeInOneServer: true,
          _tryCentralFromStore: true,
          giftcard: card
        }, function () {}, function () {
          OB.UTIL.showError("Error");
        });
      });
      };

  var checkIfExpiredVoucherAndConsume = function (keyboard, receipt, card, success, fail, options) {
      OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.FindGiftCard', {
        giftcard: card,
        giftcardtype: 'V'
      }, function (result) {
        if (result && result.data && result.data.obgcneExpirationdate && OB.OBGCNE.Utils.isInThePast(result.data.obgcneExpirationdate)) {
          var errorMessage = {
            message: OB.I18N.getLabel('OBGCNE_VoucherExpired', [card]),
            keyboard: keyboard
          };
          fail(errorMessage);
        } else {
          consumeGiftCardLines(keyboard, receipt, card, success, fail, options);
        }
      }, function (error) {
        consumeGiftCardLines(keyboard, receipt, card, success, fail, options);
      });
      };

  var cancelGiftCardTransaction = function (context, transaction) {
      // cancel transaction
      OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.CancelGiftCardTransaction', {
        _executeInOneServer: true,
        _tryCentralFromStore: true,
        transaction: transaction
      }, function (result) {
        // OK
      }, function (error) {
        // FAIL
        var msgsplit = (error.exception.message || 'GCNV_ErrorGenericMessage').split(':');
        context.doShowPopup({
          popup: 'GCNV_UI_Message',
          args: {
            message: OB.I18N.getLabel(msgsplit[0], msgsplit.slice(1))
          }
        });
      });
      };

  var getPaymentMethodCashUp = function (searchKey, callback) {
      OB.Dal.find(OB.Model.CashUp, {
        'isprocessed': 'N'
      }, function (cashUp) {
        OB.Dal.find(OB.Model.PaymentMethodCashUp, {
          'cashup_id': cashUp.at(0).get('id'),
          'searchKey': searchKey === 'BasedOnCreditNote' ? 'GCNV_payment.creditnote' : 'OBPOS_payment.giftcard'
        }, function (payMthds) { //OB.Dal.find success
          callback(payMthds.at(0));
        });
      });
      };

  var updatePaymentMethodCashUp = function (payMthd, giftcardNumber, transMsg, callback) {
      OB.Dal.save(payMthd, function () {
        OB.Dal.transaction(function (tx) {
          OB.UTIL.calculateCurrentCash(null, tx);
        }, function () {
          // the transaction failed
          OB.error("[" + transMsg + "] The transaction failed to be commited. CardNumber: " + giftcardNumber);
        }, function () {
          // success transaction...
          OB.info("[" + transMsg + "] Transaction success. CardNumber: " + giftcardNumber);
          if (callback) {
            callback();
          }
        });
      });
      };

  var hwServerPrint = function (gctemplateresource, giftCard, giftCardData, order, callback, originator) {
      OB.UTIL.HookManager.executeHooks('GCNV_PrePrintGiftCard', {
        giftCard: giftCard,
        giftCardData: giftCardData,
        order: order,
        originator: originator
      }, function () {
        if (OB.MobileApp.model.get('terminal').shfptDisbalePrint && !order.get('shfptIsDuplicatePrint')) {
            return;
        } else {
            OB.POS.hwserver.print(gctemplateresource, {
              giftCardData: giftCardData
            }, function (result) {
              if (callback) {
                callback(result);
              }
            });
        }
      });
      };

  OB.UI.GiftCardUtils = {
    findProductModel: findProductModel,
    findGiftCardModel: findGiftCardModel,
    service: service,
    cancelGiftCard: cancelGiftCard,
    cancelGiftCardTransaction: cancelGiftCardTransaction,
    consumeGiftCardAmount: consumeGiftCardAmount,
    checkIfExpiredGiftCardAndConsume: checkIfExpiredGiftCardAndConsume,
    consumeGiftCard: consumeGiftCard,
    consumeCreditNoteAmount: consumeCreditNoteAmount,
    consumeGiftCardLines: consumeGiftCardLines,
    checkIfExpiredVoucherAndConsume: checkIfExpiredVoucherAndConsume,
    getPaymentMethodCashUp: getPaymentMethodCashUp,
    updatePaymentMethodCashUp: updatePaymentMethodCashUp,
    hwServerPrint: hwServerPrint
  };

  OB.OBGCNE.Utils.isGiftCard = function (product, callback) {
    if (product.get('giftCardTransaction')) {
      // Is a giftcard product BUT as a discount payment
      callback(false);
    } else {
      var criteria;
      if (product.get('gcnvGiftcardtype')) {
        callback(true, product);
      } else {
        callback(false);
      }
    }
  };

  OB.OBGCNE.Utils.isInThePast = function (dateString) {
    var date, now;

    date = new Date(!OB.UTIL.isNullOrUndefined(dateString) ? OB.I18N.parseServerDate(dateString) : dateString);
    now = new Date();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    now.setMilliseconds(0);

    return date.getTime() < now.getTime();
  };

  OB.OBGCNE.Utils.openGiftCardSelector = function (context, giftcardId) {
    context.doShowPopup({
      popup: 'GCNV_UI_ModalGiftCards',
      args: {
        keyboard: context,
        header: OB.I18N.getLabel('GCNV_HeaderGiftCardCashMng'),
        notDefaultAction: true,
        giftcardtype: ['BasedOnCreditNote', 'BasedOnGLItem'],
        giftcardId: giftcardId,
        action: function (dialog, consumeOK, consumeFail) {
          OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.FindGiftCard', {
            giftcard: dialog.args.giftcard.searchKey,
            loadReimbursedMethods: true
          }, function (result) {
            var giftcard = result.data;

            var successCallback = function (successMessage) {
                consumeOK(null, successMessage);
                };

            var errorCallback = function (errorMessage) {
                consumeFail(null, errorMessage);
                };

            // Verify is closed
            if (giftcard.alertStatus === "C") {
              errorCallback({
                message: 'GCNV_ErrorGiftCardClosed',
                params: [giftcard.searchKey],
                keyboard: context
              });
              return;
            }

            if (giftcard.type !== 'BasedOnCreditNote') {
              // Verify is BasedOnGLItem
              if (giftcard.type !== 'BasedOnGLItem') {
                errorCallback({
                  message: 'GCNV_ErrorGiftCardNotCertificate',
                  params: [giftcard.searchKey],
                  keyboard: context
                });
                return;
              }

              // Verify it can be reimbursed
              var reasonType = _.find(OB.MobileApp.model.get('gcnvGiftcardReason'), function (reason) {
                return reason.id === giftcard.category;
              });
              if (!reasonType) {
                errorCallback({
                  message: 'GCNV_ErrorGiftCardNotFoundCategory',
                  params: [giftcard.searchKey],
                  keyboard: context
                });
                return;
              }
              if (!reasonType.reimbursed) {
                errorCallback({
                  message: 'GCNV_ErrorGiftCardNotReimbursed',
                  params: [giftcard.searchKey],
                  keyboard: context
                });
                return;
              }
            }

            // Select Cash payment method for GiftCard payment method
            var paymentMethods = [];
            _.each(giftcard.reimbursedMethods, function (method) {
              var item = _.find(OB.MobileApp.model.get('payments'), function (p) {
                return p.paymentMethod.searchKey === method.searchKey;
              });
              if (item) {
                if (item.paymentMethod.paymentMethodCategory) {
                  var category = _.find(paymentMethods, function (pm) {
                    return pm.paymentMethodCategory === item.paymentMethod.paymentMethodCategory;
                  });
                  if (!category) {
                    method.paymentMethodCategory = item.paymentMethod.paymentMethodCategory;
                    method.name = item.paymentMethod.paymentMethodCategory$_identifier;
                    method.availables = [method.searchKey];
                    method.searchKey = '';
                    paymentMethods.push(method);
                  } else {
                    category.availables.push(method.searchKey);
                  }
                } else {
                  paymentMethods.push(method);
                }
              } else {
                OB.UTIL.showError('Not found payment method: ' + method.name);
              }
            });

            if (paymentMethods.length > 1) {
              OB.MobileApp.view.waterfall('onHidePopup', {
                popup: 'GCNV_UI_Details',
                testingHide: true
              });
              OB.MobileApp.view.waterfall('onShowPopup', {
                popup: 'GCNV_UI_ModalReimbursePaymentSelector',
                args: {
                  paymentMethods: paymentMethods,
                  giftcard: giftcard,
                  consumeOK: consumeOK,
                  consumeFail: consumeFail,
                  keyboard: context
                }
              });
            } else if (paymentMethods.length === 1) {
              var searchKey = paymentMethods[0].searchKey,
                  payment = _.find(OB.POS.modelterminal.get('payments'), function (pay) {
                  return pay.paymentMethod.searchKey === searchKey;
                });
              if (payment) {
                OB.MobileApp.view.waterfall('onPaymentChanged', {
                  payment: payment,
                  status: searchKey,
                  giftcard: giftcard,
                  consumeOK: consumeOK,
                  consumeFail: consumeFail,
                  keyboard: context
                });
              }
            } else {
              errorCallback({
                message: 'GCNV_ErrorGiftCardNotFoundReimbursePaymentMethod',
                params: [giftcard.searchKey],
                keyboard: context
              });
            }
          }, function (error) {
            var msgsplit = (error.exception.message || 'GCNV_ErrorGenericMessage').split(':');
            context.doShowPopup({
              popup: 'GCNV_UI_Message',
              args: {
                message: OB.I18N.getLabel(msgsplit[0], msgsplit.slice(1))
              }
            });
          });
        }
      }
    });

  };

}());
