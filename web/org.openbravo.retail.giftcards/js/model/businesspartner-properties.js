/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

(function () {
  OB.Model.BusinessPartner.addProperties([{
    name: 'uniqueCreditNote',
    column: 'uniqueCreditNote',
    primaryKey: false,
    filter: false,
    type: 'BOOL'
  }]);
}());