/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, Backbone, enyo, _ */

// Renders a modal popup with the available payment methods for reimburse gift card 
enyo.kind({
  name: 'GCNV.UI.ModalReimbursePaymentSelector',
  kind: 'OB.UI.Modal',
  topPosition: '125px',
  events: {
    onShowPopup: ''
  },
  handlers: {
    onButtonPaymentChanged: 'buttonPaymentChanged',
    onButtonPaymentChangedCancelled: 'buttonPaymentChangedCancelled'
  },
  body: {
    kind: 'GCNV.UI.ReimbursePaymentMethods'
  },
  buttonPaymentChanged: function (inSender, inEvent) {
    if (inEvent && inEvent.status) {
      var cashupMng = this.owner,
          keyboard = inEvent.keyboard ? inEvent.keyboard : this.args.keyboard,
          consumeOK = inEvent.consumeOK ? inEvent.consumeOK : this.args.consumeOK,
          consumeFail = inEvent.consumeFail ? inEvent.consumeFail : this.args.consumeFail;

      var successCallback = function (successMessage) {
          consumeOK(null, successMessage);
          };

      var errorCallback = function (errorMessage) {
          consumeFail(null, errorMessage);
          };

      var convertAmount = function (amount, fromCurrency, toCurrency) {
          var converter = OB.UTIL.currency.findConverter(fromCurrency, toCurrency);
          if (fromCurrency === toCurrency) {
            return amount;
          } else if (converter) {
            return OB.DEC.mul(amount, converter.rate);
          } else {
            converter = OB.UTIL.currency.findConverter(fromCurrency, OB.UTIL.currency.webPOSDefaultCurrencyId());
            if (converter) {
              return OB.UTIL.currency.toForeignCurrency(toCurrency, converter.getFinancialAmountOf(amount));
            } else {
              return;
            }
          }
          };

      var getCurrentCashOf = function (payment) {
          var cashmgmtPayment = cashupMng.model.get('payments').find(function (pm) {
            return payment.payment.id === pm.get('paymentmethod_id');
          });
          if (cashmgmtPayment) {
            return cashmgmtPayment.get('total');
          } else {
            return payment.currentCash;
          }
          };

      var payment = inEvent.payment,
          giftcard = inEvent.giftcard ? inEvent.giftcard : this.args.giftcard,
          amount = giftcard.currentamount ? giftcard.currentamount : giftcard.amount,
          giftcardAmount = convertAmount(amount, giftcard.currency, payment.paymentMethod.currency);

      // Check if payment method is cash and has enough cash
      if (payment.paymentMethod.iscash) {
        if (!payment.paymentMethod.allowdrops) {
          errorCallback({
            message: 'GCNV_ErrorGiftCardNotAllowdrops',
            params: [giftcard.searchKey],
            keyboard: keyboard
          });
          return;
        }
        var availableCash = OB.UTIL.currency.toForeignCurrency(payment.paymentMethod.currency, getCurrentCashOf(payment));
        if (availableCash < giftcardAmount) {
          errorCallback({
            message: 'GCNV_ErrorGiftCardNotCash',
            params: [giftcard.searchKey],
            keyboard: keyboard
          });
          return;
        }
      }

      // Check if card are already reimbursed
      var isError = false;
      cashupMng.model.depsdropstosave.each(function (pay) {
        if (pay.get('gcnvGiftCardId') === giftcard.id) {
          isError = true;
        }
      });
      if (isError) {
        errorCallback({
          message: 'GCNV_ErrorCashMngAlreadyReimbursed',
          params: [giftcard.searchKey],
          keyboard: keyboard
        });
        return;
      }

      var criteria = {
        bpartner: giftcard.businessPartner
      };
      if (OB.MobileApp.model.hasPermission('OBPOS_remote.customer', true)) {
        var bPartnerId = {
          columns: ['bpartner'],
          operator: 'equals',
          value: giftcard.businessPartner,
          isId: true
        };
        var remoteCriteria = [bPartnerId];
        criteria.remoteFilters = remoteCriteria;
      }

      OB.Dal.find(OB.Model.BPLocation, criteria, function (dataBps) {
        if (dataBps.models[0]) {
          giftcard.address = dataBps.models[0];
        }
        // Request approval
        OB.UTIL.Approval.requestApproval(cashupMng.model, 'GCNV_PaymentGiftCardReimbursed', function (approved) {
          if (approved) {
            // Add to Cash Management
            cashupMng.currentPayment = {
              allowopendrawer: payment.paymentMethod.allowopendrawer,
              amount: giftcardAmount,
              destinationKey: payment.payment.searchKey,
              glItem: payment.paymentMethod.gLItemForDrops,
              id: payment.payment.id,
              identifier: payment.payment._identifier,
              iscash: payment.paymentMethod.iscash,
              isocode: payment.isocode,
              rate: payment.rate,
              type: "drop",
              defaultProcess: 'N',
              extendedType: 'GCNV_reimbursed',
              extendedProp: {
                gcnvGiftCardId: giftcard.id,
                giftcard: giftcard,
                allowOnlyOne: true
              }
            };

            var reimburseEvent = _.find(OB.MobileApp.model.get('cashMgmtGiftCardsEvents'), function (event) {
              return event.eventType === 'GCNV_reimbursed' && event.paymentMethodId === payment.paymentMethod.paymentMethod;
            });

            cashupMng.model.depsdropstosave.trigger('paymentDone', new Backbone.Model({
              id: reimburseEvent.id,
              name: reimburseEvent.name
            }), cashupMng.currentPayment);

            successCallback({
              message: 'GCNV_ReimbursedSuccess',
              params: [OB.I18N.formatCurrencyWithSymbol(giftcardAmount, payment.symbol, payment.currencySymbolAtTheRight)],
              keyboard: keyboard
            });

          }
        });
      }, function (tx, error) {
        OB.UTIL.showError("OBDAL error: " + error);
      });
    }
  },
  buttonPaymentChangedCancelled: function (inSender, inEvent) {
    if (inEvent.cashManagement) {
      this.show(this.args);
    }
  },
  executeOnShow: function () {
    if (this.args.paymentMethods) {
      var collection = new OB.Collection.GiftCardList();
      this.$.body.$.reimbursePaymentMethods.$.paymentMethods.setCollection(collection);
      collection.reset(this.args.paymentMethods);
    }
    this.selectItem = false;
  },
  executeOnHide: function () {
    if (!this.selectItem) {
      this.doShowPopup({
        popup: 'GCNV_UI_Details'
      });
    }
  },
  initComponents: function () {
    this.header = OB.I18N.getLabel('GCNV_LblReimbursePaymentSelector');
    this.inherited(arguments);
  }
});

enyo.kind({
  name: 'GCNV.UI.ReimbursePaymentMethods',
  classes: 'row-fluid',
  components: [{
    classes: 'span12',
    components: [{
      name: 'paymentMethods',
      kind: 'OB.UI.Table',
      style: 'overflow: auto; max-height: 600px',
      renderLine: 'GCNV.UI.ReimbursePaymentMethodsLine',
      renderEmpty: 'OB.UI.RenderEmpty'
    }]
  }]
});

enyo.kind({
  name: 'GCNV.UI.ReimbursePaymentMethodsLine',
  kind: 'OB.UI.SelectButton',
  style: 'height: 60px; background-color: #dddddd; border: 1px solid #ffffff;',
  events: {
    onShowPopup: '',
    onHideThisPopup: ''
  },
  components: [{
    name: 'line',
    style: 'padding: 1px 0px 1px 5px;'
  }],
  tap: function () {
    this.inherited(arguments);
    var dialog = this.owner.owner.owner.owner.owner.owner,
        amount = dialog.args.giftcard.currentamount ? dialog.args.giftcard.currentamount : dialog.args.giftcard.amount;
    dialog.selectItem = true;
    this.doHideThisPopup();
    if (this.model.get('paymentMethodCategory')) {
      this.doShowPopup({
        popup: 'modalPaymentsSelect',
        args: {
          idCategory: this.model.get('paymentMethodCategory'),
          availables: this.model.get('availables'),
          cashManagement: true,
          amount: amount
        }
      });
    } else {
      var searchKey = this.model.get('searchKey'),
          payment = _.find(OB.POS.modelterminal.get('payments'), function (pay) {
          return pay.paymentMethod.searchKey === searchKey;
        });
      if (payment) {
        this.bubble('onPaymentChanged', {
          payment: payment,
          status: searchKey,
          amount: amount
        });
      }
    }
  },
  create: function () {
    this.inherited(arguments);
    this.$.line.setContent(this.model.get('name'));
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSCashMgmt.UI.CashManagement', {
  kind: 'GCNV.UI.ModalReimbursePaymentSelector',
  name: 'GCNV_UI_ModalReimbursePaymentSelector'
});

OB.UI.WindowView.registerPopup('OB.OBPOSCashMgmt.UI.CashManagement', {
  kind: 'OB.OBPOSPointOfSale.UI.Modals.ModalPaymentsSelect',
  name: 'modalPaymentsSelect'
});
