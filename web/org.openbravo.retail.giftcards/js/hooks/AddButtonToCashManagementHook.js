/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_AddButtonToCashManagement', function (args, callbacks) {
    var cashMgmtGiftCardsEvents = OB.MobileApp.model.get('cashMgmtGiftCardsEvents'),
        reimburseEvent = _.find(cashMgmtGiftCardsEvents, function (event) {
        return event.eventType === 'GCNV_reimbursed';
      });

    if (reimburseEvent) {

      // Add a button GiftCard Reimbursed
      args.buttons.push({
        idSufix: 'GiftCard',
        command: 'OBPOS_payment.giftcard',
        label: OB.I18N.getLabel('GCNV_BtnGiftCardCashMng'),
        definition: {
          stateless: true,
          permission: 'OBPOS_PaymentGiftCard',
          action: function (keyboard, txt) {
            OB.OBGCNE.Utils.openGiftCardSelector(args.context);
          }
        }
      });
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());
