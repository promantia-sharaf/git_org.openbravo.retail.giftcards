/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PreOrderSave', function (args, callbacks) {
    args.receipt.get('payments').forEach(function (p) {
      if (p.get('kind') === 'GCNV_payment.creditnote' && OB.MobileApp.model.hasPermission('GCNV_ForbidAnonymousCustomerUseCreditNote', true)) {
        if (args.receipt.get('bp').id === OB.MobileApp.model.get('businessPartner').id) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('GCNV_ForbidAnonymousCustomerUseCreditNote'));
          args.cancellation = true;
        } else if (args.receipt.get('bp').id !== p.get('paymentData').creditNote.businessPartnerId) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('GCNV_ForbidUseCreditNoteWithDifferentCustomer'));
          args.cancellation = true;
        }
      }
    });
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());
