/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_postAddPayment', function (args, callbacks) {
    if ('GCNV_payment.creditnote' === args.paymentAdded.get('kind')) {
      _.each(args.payments.models, function (payment) {
        if ('GCNV_payment.creditnote' === payment.get('kind') && args.receipt.get('documentNo') === payment.get('paymentData').groupingCriteria) {
          payment.get('paymentData').creditNote.amount = payment.get('amount');
        }
      });
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());