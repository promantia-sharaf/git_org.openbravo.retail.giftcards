/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBMOBC_PrePaymentSelected', function (args, callbacks) {
    if (OB.MobileApp.model.hasPermission('GCNV_ForbidAnonymousCustomerUseCreditNote', true) && args.paymentSelected && args.paymentSelected.name === 'GCNV_payment.creditnote' && args.receipt.get('bp').id === OB.MobileApp.model.get('businessPartner').id) {
      if (args.paymentSelected.definition.includedInPopUp) {
        args.paymentSelected.owner.owner.owner.owner.hide();
      }
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('GCNV_ForbidAnonymousCustomerUseCreditNote'));
      args.cancellation = true;
    }

    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());