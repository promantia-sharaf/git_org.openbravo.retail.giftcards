/*
 ************************************************************************************
 * Copyright (C) 2015-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _, $ */


(function () {

  var printGiftCards, getPaymentGiftCards, printLineGiftCards, printPaymentGiftCards;

  printGiftCards = function (receiptList, iteration, hookCallback) {
    printLineGiftCards(receiptList, 0, function () {
      getPaymentGiftCards(receiptList, function () {
        hookCallback();
      });
    });
  };

  printLineGiftCards = function (receiptList, iteration, callback) {
    if (receiptList.length - 1 >= iteration) {
      var getReceiptLine, receipt = receiptList[iteration];

      getReceiptLine = function (lineIteration) {
        if (receipt.get('lines').length - 1 >= lineIteration) {
          var line = receipt.get('lines').models[lineIteration],
              gctemplateresource, giftCardData = new Backbone.Model();

          if (line.get('giftcardid') && line.get('product').get('printCard') && line.get('product').get('printTemplate')) {
            gctemplateresource = new OB.DS.HWResource(line.get('product').get('printTemplate'));

            // Set properties to giftCardData in order to print it
            giftCardData.set('giftCardId', line.get('giftcardid'));
            giftCardData.set('expirationDate', (line.get('giftcardexpirationdate') || line.get('giftcardobgcneExpirationdate')));
            giftCardData.set('productId', line.get('product').id);
            giftCardData.set('productName', line.get('product').get('_identifier'));
            giftCardData.set('businessPartnerId', receipt.get('bp').id);
            giftCardData.set('businessPartnerName', receipt.get('bp').get('_identifier'));
            giftCardData.set('gcOwnerId', (line.get('gcowner') || line.get('giftcardobgcneGCOwner')));
            giftCardData.set('gcOwnerName', line.get('gcowner_name'));

            // Find GiftCard in local database
            OB.UI.GiftCardUtils.findGiftCardModel(line.get('product').get('id'), function (giftcardproduct) {
              // Add giftcard properties to giftCardData
              giftCardData.set('giftCardType', giftcardproduct.get('giftCardType'));
              giftCardData.set('amount', giftcardproduct.get('amount'));

              if (line.get('product').get('templateIsPdf')) {
                if (line.get('product').get('templatePrinter')) {
                  gctemplateresource.printer = parseInt(line.get('product').get('templatePrinter'), 10);
                  gctemplateresource.dateFormat = OB.Format.date;
                  gctemplateresource.subreports = [];
                  gctemplateresource.getData(function () {
                    OB.POS.hwserver._printPDF({
                      param: JSON.parse(JSON.stringify(giftCardData.toJSON())),
                      mainReport: gctemplateresource,
                      subReports: gctemplateresource.subreports
                    }, getReceiptLine(lineIteration + 1));
                  });
                } else {
                  // If there is no printer defined show error
                  OB.UTIL.showError(OB.I18N.getLabel('OBPGC_NoPrinter'));
                  getReceiptLine(lineIteration + 1);
                }
              } else {
                OB.UI.GiftCardUtils.hwServerPrint(gctemplateresource, {
                  type: giftcardproduct.get('giftCardType'),
                  searchKey: giftcardproduct.get('searchKey')
                }, giftCardData, receipt, getReceiptLine(lineIteration + 1));
              }
            });
          } else {
            getReceiptLine(lineIteration + 1);
          }
        } else {
          printLineGiftCards(receiptList, iteration + 1, callback);
        }
      };
      getReceiptLine(0);
    } else {
      callback();
    }
  };

  getPaymentGiftCards = function (receiptList, callback) {
    var giftCardPayments = [];
    _.each(receiptList, function (receipt) {
      _.each(receipt.get('payments').models, function (payment) {
        if (payment.get('kind') === 'OBPOS_payment.giftcard' && !payment.get('isPrePayment') && payment.get('paymentData').card) {
          if (!_.find(giftCardPayments, function (gc) {
            return gc.giftCard && gc.id === payment.get('paymentData').card;
          })) {
            giftCardPayments.push({
              id: payment.get('paymentData').card,
              giftCard: true,
              receipt: receipt,
              payment: payment
            });
          }
        } else if (payment.get('kind') === 'GCNV_payment.creditnote' && !payment.get('isPrePayment')) {
          var giftCardPayment = _.find(giftCardPayments, function (gc) {
            return gc.creditNote && gc.id === payment.get('paymentData').creditNote.searchKey;
          });
          if (giftCardPayment) {
            giftCardPayment.payment.set('amount', OB.DEC.add(giftCardPayment.payment.get('amount'), payment.get('amount')));
            giftCardPayment.payment.set('origAmount', OB.DEC.add(giftCardPayment.payment.get('origAmount'), payment.get('origAmount')));
            giftCardPayment.payment.set('paid', OB.DEC.add(giftCardPayment.payment.get('paid'), payment.get('paid')));
          } else {
            giftCardPayments.push({
              id: payment.get('paymentData').creditNote.searchKey,
              creditNote: true,
              receipt: receipt,
              payment: payment
            });
          }
        }
      });
    });
    printPaymentGiftCards(giftCardPayments, 0, function () {
      callback();
    });
  };

  printPaymentGiftCards = function (giftCardPayments, iteration, callback) {
    if (giftCardPayments.length - 1 >= iteration) {
      var receipt, payment, gctemplateresource, giftCardData, giftCard;

      receipt = giftCardPayments[iteration].receipt;
      payment = giftCardPayments[iteration].payment;
      giftCardData = new Backbone.Model();
      if (payment.get('kind') === 'OBPOS_payment.giftcard' && !payment.get('isPrePayment')) {
        OB.UI.GiftCardUtils.service('org.openbravo.retail.giftcards.FindGiftCard', {
          giftcard: payment.get('paymentData').card
        }, function (result) {

          giftCard = result.data;

          // Find Gift Card Reason in Terminal Properties
          var reasonType = _.find(OB.MobileApp.model.get('gcnvGiftcardReason'), function (reason) {
            return reason.id === giftCard.category;
          });

          if (reasonType.printCard && reasonType.printTemplate && giftCard.currentamount > 0) {
            gctemplateresource = new OB.DS.HWResource(reasonType.printTemplate);

            // Set properties to giftCardData in order to print it
            giftCardData.set('giftCardId', giftCard.searchKey);
            giftCardData.set('expirationDate', OB.I18N.formatDate(new Date(!OB.UTIL.isNullOrUndefined(giftCard.obgcneExpirationdate) ? OB.I18N.parseServerDate(giftCard.obgcneExpirationdate) : giftCard.obgcneExpirationdate)));
            giftCardData.set('businessPartnerId', giftCard.businessPartner);
            giftCardData.set('businessPartnerName', giftCard.businessPartner$_identifier);
            giftCardData.set('gcOwnerId', giftCard.obgcneGCOwner);
            giftCardData.set('gcOwnerName', giftCard.obgcneGCOwner$_identifier);
            giftCardData.set('amount', giftCard.amount);
            giftCardData.set('currentamount', giftCard.currentamount);
            giftCardData.set('alertStatus', giftCard.alertStatus);
            giftCardData.set('type', giftCard.type);
            giftCardData.set('categoryId', giftCard.category);
            giftCardData.set('categoryName', giftCard.category$_identifier);

            if (reasonType.templateIsPdf) {
              if (reasonType.templatePrinter) {
                gctemplateresource.printer = parseInt(reasonType.templatePrinter, 10);
                gctemplateresource.dateFormat = OB.Format.date;
                gctemplateresource.subreports = [];
                gctemplateresource.getData(function () {
                  OB.POS.hwserver._printPDF({
                    param: JSON.parse(JSON.stringify(giftCardData.toJSON())),
                    mainReport: gctemplateresource,
                    subReports: gctemplateresource.subreports
                  }, printPaymentGiftCards(giftCardPayments, iteration + 1, callback));
                });
              } else {
                // If there is no printer defined show error
                OB.UTIL.showError(OB.I18N.getLabel('OBPGC_NoPrinter'));
                printPaymentGiftCards(giftCardPayments, iteration + 1, callback);
              }
            } else {
              OB.UI.GiftCardUtils.hwServerPrint(gctemplateresource, giftCard, giftCardData, receipt, printPaymentGiftCards(giftCardPayments, iteration + 1, callback));
            }
          } else {
            printPaymentGiftCards(giftCardPayments, iteration + 1, callback);
          }
        }, function (error) {
          var msgsplit = (error.exception.message || 'GCNV_ErrorGenericMessage').split(':');
          OB.MobileApp.view.$.containerWindow.getRoot().doShowPopup({
            popup: 'GCNV_UI_Message',
            args: {
              message: OB.I18N.getLabel(msgsplit[0], msgsplit.slice(1))
            }
          });
        });
      } else if (payment.get('kind') === 'GCNV_payment.creditnote' && !payment.get('isPrePayment')) {
        var printCreditNote, creditNoteData, currentAmt = null,
            templateCreditNote = OB.MobileApp.model.get('terminal').printCreditNoteTemplate;
        gctemplateresource = new OB.DS.HWResource(templateCreditNote || OB.OBGCNE.Utils.PrintCreditNoteTemplate);

        printCreditNote = function (creditNote, currentAmount) {
          if (currentAmount === OB.DEC.Zero) {
            printPaymentGiftCards(giftCardPayments, iteration + 1, callback);
            return;
          }
          giftCardData.set('giftCardId', creditNote.searchKey);
          giftCardData.set('businessPartnerId', creditNote.businessPartnerId);
          giftCardData.set('businessPartnerName', creditNote.businessPartnerName);
          giftCardData.set('amount', creditNote.amount);
          giftCardData.set('currentamount', currentAmount);
          
          OB.UI.GiftCardUtils.hwServerPrint(gctemplateresource, {
            type: 'BasedOnCreditNote',
            searchKey: creditNote.searchKey
          }, giftCardData, receipt, function () {
            printPaymentGiftCards(giftCardPayments, iteration + 1, callback);
          });
        };
        if (receipt.get('bp').get('uniqueCreditNote') && OB.MobileApp.model.get('terminal').businessPartner !== receipt.get('bp').get('id')) {
          var serverCall = new OB.DS.Process('org.openbravo.retail.giftcards.FindCreditNote');
          serverCall.exec({
            bp: receipt.get('bp')
          }, function (data) {
            if (data && data.exception) {
              OB.UTIL.showConfirmation.display('', data.exception.message);
            } else if (data.searchKey) {
              creditNoteData = data;
              currentAmt = data.currentAmount;
            } else {
              if (!OB.UTIL.isNullOrUndefined(payment.get('paymentData').creditNote.currentamount)) {
                currentAmt = OB.DEC.sub(payment.get('paymentData').creditNote.currentamount, payment.get('origAmount'));
              }
              creditNoteData = payment.get('paymentData').creditNote;
            }
            if (receipt.getPaymentStatus().isNegative && currentAmt) {
              currentAmt = OB.DEC.add(payment.get('origAmount'), currentAmt);
            }
            printCreditNote(creditNoteData, currentAmt);
          });
        } else {
          if (!OB.UTIL.isNullOrUndefined(payment.get('paymentData').creditNote.currentamount)) {
            currentAmt = OB.DEC.sub(payment.get('paymentData').creditNote.currentamount, payment.get('origAmount'));
          }
          printCreditNote(payment.get('paymentData').creditNote, currentAmt);
        }
      } else {
        printPaymentGiftCards(giftCardPayments, iteration + 1, callback);
      }
    } else {
      callback();
    }
  };

  OB.UTIL.HookManager.registerHook('OBPRINT_PrePrint', function (args, callbacks) {
    var hookCallback;
    hookCallback = function () {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    };
    if (args.model.get('leftColumnViewManager').isOrder()) {
      printGiftCards([args.order], 0, hookCallback);
    } else if (args.model.get('leftColumnViewManager').isMultiOrder()) {
      if (args.order.get('id') === _.last(args.model.get('multiOrders').get('multiOrdersList').models).get('id')) {
        printGiftCards(args.model.get('multiOrders').get('multiOrdersList').models, 0, hookCallback);
      } else {
        hookCallback();
      }
    } else {
      hookCallback();
    }
  });

}());
