/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_AddPaymentToCashManagement', function (args, callbacks) {

    var payment = _.find(OB.POS.modelterminal.get('payments'), function (p) {
      return p.paymentMethod.searchKey === args.paymentMethod.searchKey;
    });
    var reimburseEvent = _.find(OB.MobileApp.model.get('cashMgmtGiftCardsEvents'), function (event) {
      return event.paymentMethodId === payment.paymentMethod.paymentMethod;
    });
    if (!OB.UTIL.isNullOrUndefined(reimburseEvent)) {
      args.context.get('payments').add(args.pay);
    }
    args.includePay = !OB.UTIL.isNullOrUndefined(reimburseEvent);
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());