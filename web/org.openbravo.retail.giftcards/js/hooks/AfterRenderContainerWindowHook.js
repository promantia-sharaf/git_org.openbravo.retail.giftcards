/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_AfterRenderContainerWindow', function (args, callbacks) {

    if (args.window === 'retail.cashmanagement' && args.parameters && args.parameters._entityName === 'GCNV_GiftCardInst' && (args.parameters.type === 'BasedOnGLItem' || args.parameters.type === 'BasedOnCreditNote')) {
      var names = _.keys(args.context.$),
          cashMgmtKeyboard = args.context.$[names[0]].$.cashupMultiColumn.$.rightPanel.$.cashMgmtKeyboard;
      OB.OBGCNE.Utils.openGiftCardSelector(cashMgmtKeyboard, args.parameters.searchKey);
    }

    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());