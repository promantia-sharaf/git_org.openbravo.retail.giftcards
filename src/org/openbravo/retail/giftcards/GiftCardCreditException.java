/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.retail.giftcards;

public class GiftCardCreditException extends Exception {

  private static final long serialVersionUID = 1L;

  public GiftCardCreditException(String msg) {
    super(msg);
  }
}
