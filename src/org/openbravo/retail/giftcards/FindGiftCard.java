/*
 ************************************************************************************
 * Copyright (C) 2012-2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.retail.giftcards;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.giftcards.org.openbravo.retail.giftcards.GiftCardInst;
import org.openbravo.service.json.DataResolvingMode;
import org.openbravo.service.json.JsonConstants;

public class FindGiftCard extends AbstractSimpleGiftCardProcess {

  @Override
  protected JSONObject execute(JSONObject json) {
    GiftCardModel model = new GiftCardModel();
    final JSONObject result = new JSONObject();
    try {
      try {
        final GiftCardInst giftcard = model.findGiftCard(json.getString("giftcard"),
            json.optString("giftcardtype", null));

        final DataToJsonConverterExt toJsonConverter = OBProvider.getInstance().get(
            DataToJsonConverterExt.class);

        final JSONObject jsongiftcard = toJsonConverter.toJsonObject(giftcard,
            DataResolvingMode.FULL);

        if (giftcard != null) {
          if ("BasedOnGLItem".equals(giftcard.getType())) {
            Organization org = OBContext.getOBContext().getCurrentOrganization();
            if (giftcard.getCategory().isOnlyOrg()) {
              if (!giftcard.getOrganization().getId().equals(org.getId())) {
                throw new Exception("GCNV_ErrorGiftCardNotExists:" + giftcard.getSearchKey());
              }
            }
          }
          if ("BasedOnGLItem".equals(giftcard.getType())
              || "BasedOnCreditNote".equals(giftcard.getType())) {
            Boolean loadReimbursedMethods = json.has("loadReimbursedMethods") ? json
                .getBoolean("loadReimbursedMethods") : false;
            if (loadReimbursedMethods) {
              final String sqlString = "SELECT pmtype.paymentMethod.searchKey, pmtype.paymentMethod.name "
                  + "FROM OBPOS_Applications term JOIN term.oBPOSAppPaymentList pmtype "
                  + "WHERE term.id = :termId AND pmtype.active = true AND pmtype.paymentMethod.paymentMethod.id IN "
                  + "(SELECT cme.paymentMethod.id FROM OBRETCO_CashManagementEvents cme "
                  + "WHERE cme.organization.id = term.organization.id AND cme.paymentMethod.id = pmtype.paymentMethod.paymentMethod.id"
                  + " AND cme.active = true AND cme.eventtype = 'GCNV_reimbursed') order by pmtype.line, pmtype.commercialName";
              final Session session = OBDal.getInstance().getSession();
              final Query query = session.createQuery(sqlString);
              query.setParameter("termId", json.getString("pos"));
              JSONArray reimbursedMethods = new JSONArray();
              final List<Object[]> paymentMethods = query.list();
              for (Object[] item : paymentMethods) {
                JSONObject method = new JSONObject();
                method.put("searchKey", (String) item[0]);
                method.put("name", (String) item[1]);
                reimbursedMethods.put(method);
              }
              jsongiftcard.put("reimbursedMethods", reimbursedMethods);
            }
          }
        }

        if (giftcard.getPayment() != null) {
          jsongiftcard.put("currency", giftcard.getPayment().getAccount().getCurrency().getId());
        }

        result.put(JsonConstants.RESPONSE_DATA, jsongiftcard);
        result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
      } catch (Exception e) {
        result.put(JsonConstants.RESPONSE_ERROR, e.getMessage());
        result.put("ignoreForClientLog", true);
        result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
      }
    } catch (JSONException je) {
      // Ignore
    }

    return result;
  }

  @Override
  protected String getImportEntryDataType() {
    return null;
  }

  @Override
  protected void createImportEntry(String messageId, JSONObject sentIn, JSONObject processResult,
      Organization organization) throws JSONException {
    // We don't want to create any import entry in these transactions.
  }

  @Override
  protected void createArchiveEntry(String id, JSONObject json) throws JSONException {
    // We don't want to create any import entry in these transactions.
  }

  @Override
  protected boolean executeInOneServer(JSONObject json) throws JSONException {
    return true;
  }

}
