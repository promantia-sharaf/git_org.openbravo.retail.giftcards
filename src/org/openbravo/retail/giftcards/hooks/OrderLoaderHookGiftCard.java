/*
 ************************************************************************************
 * Copyright (C) 2013-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.retail.giftcards.hooks;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;

import javax.enterprise.context.ApplicationScoped;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.giftcards.GiftCardModel;
import org.openbravo.retail.giftcards.org.openbravo.retail.giftcards.GiftCardInst;
import org.openbravo.retail.giftcards.org.openbravo.retail.giftcards.GiftCardTrans;
import org.openbravo.retail.posterminal.OrderLoaderPaymentHook;

@ApplicationScoped
public class OrderLoaderHookGiftCard extends OrderLoaderPaymentHook {

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {
    BusinessPartner bp = order.getBusinessPartner();
    GiftCardHookUtils.updateGCPaymentTransaction(order, jsonorder, paymentSchedule);
    JSONArray orderlines = jsonorder.getJSONArray("lines");
    GiftCardModel gc = new GiftCardModel();
    for (int i = 0; i < orderlines.length(); i++) {
      JSONObject line = orderlines.getJSONObject(i);
      OrderLine orderline = order.getOrderLineList().get(i);
      String transaction = line.getJSONObject("product").optString("giftCardTransaction", null);
      if (transaction == null) {
        // Is not a gift card transaction...

        Product product = OBDal.getInstance().get(Product.class,
            line.getJSONObject("product").getString("id"));

        String giftcardid = line.optString("giftcardid", null);
        // BigDecimal amount = jsonsent.has("amount") ? new BigDecimal(jsonsent.getString("amount"))
        // : null;

        OBQuery<GiftCardInst> giftCardInstQry = OBDal.getInstance().createQuery(GiftCardInst.class,
            " as gc where gc." + GiftCardInst.PROPERTY_SALESORDERLINE + ".id = :orderLineId ");
        giftCardInstQry.setNamedParameter("orderLineId", orderline.getId());
        giftCardInstQry.setMaxResult(1);
        GiftCardInst giftCardInst = giftCardInstQry.uniqueResult();

        if (giftCardInst != null) {
          if ("D".equals(giftCardInst.getAlertStatus())
              && orderline.getOrderedQuantity().equals(orderline.getDeliveredQuantity())) {
            giftCardInst.setAlertStatus("N");
            OBDal.getInstance().save(giftCardInst);
            OBDal.getInstance().flush();
          }
          continue;
        }

        @SuppressWarnings("rawtypes")
        Iterator keys = line.keys();
        JSONObject giftCardProperties = new JSONObject();
        while (keys.hasNext()) {
          String key = (String) keys.next();
          if (key.startsWith("giftcard") && !key.equals("giftcardid")) {
            giftCardProperties.put(key.replace("giftcard", ""), line.get(key));
          }
        }

        if (!orderline.getOrderedQuantity().equals(orderline.getDeliveredQuantity())) {
          giftCardProperties.put("alertStatus", "D");
        }

        gc.createGiftCard(giftcardid, product, bp, order.getOrderDate(), order, orderline,
            orderline.getLineNetAmount(), giftCardProperties);
      } else {
	 // check if the transaction is created or not (Simple Gift Card)
        GiftCardTrans trans = transaction != null
            ? OBDal.getInstance().get(GiftCardTrans.class, transaction)
            : null;
        if (trans == null) {
          BigDecimal currentamt = new BigDecimal(
              line.getJSONObject("product").optString("currentamt", null));
          trans = gc.consumeAmountGiftCard(transaction,
              OBDal.getInstance()
                  .get(GiftCardInst.class, line.getJSONObject("product").optString("cardId", null)),
              new Date(), order, orderline, currentamt.negate(),
              line.getJSONObject("product").optBoolean("isReturn"),
              transaction == null || "null".equals(transaction) ? null : transaction, true);
        }
        // is a gift card transaction so just fill in the order and order-line fields
        gc.populateGiftCardTransaction(transaction, order, orderline);
      }
    }
  }
}
