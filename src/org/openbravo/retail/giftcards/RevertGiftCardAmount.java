/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.retail.giftcards;

import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.giftcards.org.openbravo.retail.giftcards.GiftCardTrans;

public class RevertGiftCardAmount extends AbstractSimpleGiftCardProcess {

  @Override
  public JSONObject execute(JSONObject jsonsent) {
    JSONObject result = new JSONObject();
    GiftCardModel model = new GiftCardModel();
    String transid;
    try {
      result.put("success", true);
      if (!jsonsent.optString("transaction").isEmpty()) {
        transid = jsonsent.optString("transaction");
        if (OBDal.getInstance().get(GiftCardTrans.class, jsonsent.optString("transaction")) == null) {
          result.put("giftCardTransactionNotExists", true);
          return result;
        }
      } else {
        transid = findGiftCardTransaction(jsonsent.optString("giftcard"));
      }
      model.revertGiftCardTrans(transid);
    } catch (Exception e) {
      throw new OBException(e.getMessage());
    }
    return result;
  }

  @Override
  protected String getImportEntryDataType() {
    return null;
  }

  @Override
  protected void createImportEntry(String messageId, JSONObject sentIn, JSONObject processResult,
      Organization organization) throws JSONException {
    // We don't want to create any import entry in these transactions.
  }

  @Override
  protected void createArchiveEntry(String id, JSONObject json) throws JSONException {
    // We don't want to create any import entry in these transactions.
  }

  @Override
  protected String getProperty() {
    return "GCNV_PaymentGiftCard";
  }

  private String findGiftCardTransaction(String giftcardId) {
    String whereClause = "gcnvGiftcardInst.searchKey = :giftcardId";
    String transactionId = "";
    OBQuery<GiftCardTrans> transactionGiftCard = OBDal.getInstance().createQuery(
        GiftCardTrans.class, whereClause);
    transactionGiftCard.setNamedParameter("giftcardId", giftcardId);
    List<GiftCardTrans> transactionGiftCardList = transactionGiftCard.list();
    if (transactionGiftCardList.size() > 0) {
      transactionId = transactionGiftCardList.get(0).getId();
    }
    return transactionId;
  }
}
