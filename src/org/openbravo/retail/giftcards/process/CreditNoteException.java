package org.openbravo.retail.giftcards.process;

import org.openbravo.base.exception.OBException;

public class CreditNoteException extends OBException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public CreditNoteException(String message) {
    super(message);
  }

}
